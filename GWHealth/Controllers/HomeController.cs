﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using GWHealth.Models;
using System.DirectoryServices;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using System.Text;
using System.Security.Principal;

namespace GWHealth.Controllers
{

    public class HomeController : Controller
    {

        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {

            return View(new LoginViewModel());

        }

        public IActionResult Privacy()
        {

            LoginViewModel lv = getAuthenticatedModel();
            return View(lv);

        }

        public IActionResult About()
        {
            LoginViewModel lv = getAuthenticatedModel();
            return View(lv);
        }




        public IActionResult SQL(string id, string caller)
        {
            LoginViewModel lv = getAuthenticatedModel();

            if (lv.IsAuthenticated)
            {
                lv.SetActiveEnvirnment(id);

                if (caller == "Upload")
                {
                    ViewBag.Message = "Files successfully uploaded";
                }

                return View("SqlView", lv);
            }
            else
            {
                return View("Index", lv);
            }

        }

        public IActionResult Dataload(string id, string caller)
        {
            LoginViewModel lv = getAuthenticatedModel();

            if (lv.IsAuthenticated)
            {
                lv.SetActiveEnvirnment(id);

                if (caller == "Upload")
                {
                    ViewBag.Message = "Files successfully uploaded";
                }


                return View(lv);
            }
            else
            {
                return View("Index", lv);
            }

        }

        
        [HttpGet]
        public IActionResult Env(string id)
        {

            LoginViewModel lv = getAuthenticatedModel();

            if (lv.IsAuthenticated)
            {
                lv.SetActiveEnvirnment(id);

                return View(lv);
            }
            else
            {
                return View("Index", lv);
            }


        }



        [HttpGet]
        public IActionResult Command(string id)
        {
            LoginViewModel lv = getAuthenticatedModel();

            string[] input = id.Split("%2F");

            if (lv.IsAuthenticated)
            {
                lv.SetActiveEnvirnment(input[0]);

                TCPClient.TCPClient tc = new TCPClient.TCPClient();

                string output = tc.StartClient ( lv.Username , input[1] , input[2]);
                
                ViewBag.Message = input[1] + " successfully Started ---- " + output ;
                tc = null;

                return View("Env" ,lv);
            }
            else
            {
                return View("Index", lv);
            }


        }





        public async Task<IActionResult> MainAsync()
        {
            if (Request.Cookies.Count > 0)
            {

                if (Request.Cookies["Auth"] != null)
                {
                    LoginViewModel lv = getAuthenticatedModel();
                    if (lv.IsAuthenticated)
                    {
                        return View("../Home/Main", lv);
                    }
                    else
                    {
                        return View("Index", lv);
                    }

                }
                string username = Request.Form["Username"];
                string password = Request.Form["Password"];
                if (isAuthenticated(username, password))
                {
                    LoginViewModel lv = new LoginViewModel();

                    string srvr = "LDAP://mfb.zengwcloud.com";
                    DirectoryEntry entry = new DirectoryEntry(srvr, username, password);
                    DirectorySearcher mySearcher = new DirectorySearcher(entry);
                    mySearcher.Filter = "(&(objectClass=user)(anr=" + username + "))";
                    foreach (SearchResult singleADUser in mySearcher.FindAll())
                    {
                        // Debug.WriteLine("The properties of the " + singleADUser.GetDirectoryEntry().Name + " are :");

                        lv.DisplayName = singleADUser.GetDirectoryEntry().Name.Replace("CN=", "");

                        //foreach (string singleAttribute in ((ResultPropertyCollection)singleADUser.Properties).PropertyNames)
                        //{
                        //    Debug.WriteLine(singleAttribute + " = ");
                        //    foreach (Object singleValue in ((ResultPropertyCollection)singleADUser.Properties)[singleAttribute])
                        //    {
                        //        Debug.WriteLine("\t" + singleValue);
                        //    }
                        //}


                    }


                    await HttpContext.SignInAsync("Cookies", getPrincipal(username, lv.DisplayName));
                    lv.Username = entry.Username;
                    lv.IsAuthenticated = true;
                    return View("../Home/Main", lv);
                }
                else
                {
                    //Password Failed
                    await HttpContext.SignOutAsync();
                    return View("Error", new LoginViewModel { RequestId = "404" ,  });
                }
            }
            else
            {

                LoginViewModel lv = getAuthenticatedModel();

                if (lv.IsAuthenticated)
                {
                    return View("../Home/Main", lv);
                }
                else
                {
                    return View("Index", lv);
                }



            }

        }

        private LoginViewModel getAuthenticatedModel()
        {
            if (HttpContext.User.Claims.ToList().Count > 0)
            {
                var userId = HttpContext.User.Claims.FirstOrDefault().Value;
                var DisplayName = HttpContext.User.Claims.ToList()[1].Value;
                LoginViewModel lv = new LoginViewModel();
                lv.Username = userId;
                lv.DisplayName = DisplayName;
                lv.IsAuthenticated = true;
                return lv;
            }
            else
            {
                return new LoginViewModel();
            }

        }

        private ClaimsPrincipal getPrincipal(string username, string displayname)
        {
            var claims = new List<Claim>
                    {
                     new Claim( "User", username  ) ,
                     new Claim( "DisplayName", displayname )
                     };

            var userIdentity = new ClaimsIdentity(claims, "User");
            ClaimsPrincipal principal = new ClaimsPrincipal(userIdentity);

            return principal;

        }

        private bool isAuthenticated(string username, string password)
        {
            string srvr = "LDAP://mfb.zengwcloud.com";
            bool authenticated = false;

            try
            {
                DirectoryEntry entry = new DirectoryEntry(srvr, username, password);
                object nativeObject = entry.NativeObject;
                authenticated = true;
            }
            catch (DirectoryServicesCOMException cex)
            {
                return false;

            }
            catch (Exception ex)
            {
                return false;

            }

            return authenticated;
        }

        


        public async Task<IActionResult> SignOutAsync()
        {

            await HttpContext.SignOutAsync();

            LoginViewModel lv = new LoginViewModel();
            return View(lv);
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new LoginViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
